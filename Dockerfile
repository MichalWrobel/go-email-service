FROM golang:1.15.2-alpine3.12
ENV GO111MODULE=on \
    GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0
RUN apk add --no-cache git && \
    apk add --no-cache curl && \
    apk add --no-cache jq

COPY / /go/src/assignment
RUN download_url=$(curl -s https://api.github.com/repos/go-swagger/go-swagger/releases/27583856 | \
    jq -r '.assets[] | select(.name | contains("'"$(uname | tr '[:upper:]' '[:lower:]')"'_amd64")) | .browser_download_url') && \
    curl -o /usr/local/bin/swagger -L'#' "$download_url" && \
    chmod +x /usr/local/bin/swagger
WORKDIR /go/src/assignment/swagger
RUN swagger generate server
WORKDIR /go/src/assignment
RUN go build -o assignment ./swagger/cmd/assignment-server/main.go

FROM scratch
ENV SMTP_USER=testgolangapptest@gmail.com \
    SMTP_PASSWORD=Zaq123edc? \
    SMTP_HOST=smtp.gmail.com \
    SMTP_PORT=587 \
    DB_HOST=cassandra00
COPY --from=0 /go/src/assignment/assignment /assignment
EXPOSE 8080
ENTRYPOINT ["/assignment", "--port", "8080"]
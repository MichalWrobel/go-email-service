Start application with:

docker network create assignment && \
docker run --name cassandra -p 9042:9042 --net=assignment cassandra && \
docker run --env DB_HOST=cassandra --name=assignment -p 8080:8080 --net=assignment michalwrobel2/assignment:v1


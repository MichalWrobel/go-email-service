module acaisoft

go 1.14

require (
	github.com/go-openapi/errors v0.19.6
	github.com/go-openapi/loads v0.19.5
	github.com/go-openapi/runtime v0.19.19
	github.com/go-openapi/spec v0.19.8
	github.com/go-openapi/strfmt v0.19.5
	github.com/go-openapi/swag v0.19.9
	github.com/go-openapi/validate v0.19.10
	github.com/go-swagger/go-swagger v0.24.0 // indirect
	github.com/gocql/gocql v0.0.0-20200624222514-34081eda590e
	github.com/jessevdk/go-flags v1.4.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344
)

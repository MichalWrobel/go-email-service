package message

import (
	"acaisoft/swagger/models"
	"os"
	"time"

	"github.com/gocql/gocql"
)

type connection struct {
	session *gocql.Session
}

// NewCassandraConnection creates Cassandra connection instance
func NewCassandraConnection() (Messages, error) {
	cluster := gocql.NewCluster(os.Getenv("DB_HOST"))
	cluster.ProtoVersion = 3
	s, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	err = createKeyspace(s)
	if err != nil {
		return nil, err
	}
	cluster.Keyspace = "assignment"
	return &connection{
		session: s,
	}, err
}

func createKeyspace(session *gocql.Session) (err error) {
	err = session.Query("CREATE KEYSPACE IF NOT EXISTS assignment WITH REPLICATION = {'class' : 'NetworkTopologyStrategy', 'datacenter1' : 1};").Exec()
	if err != nil {
		return
	}

	return session.Query("ALTER KEYSPACE assignment WITH replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };").Exec()
}

// InsertMessage inserts single email message into database
func (c *connection) InsertMessage(msg *models.Message) (timeUUID gocql.UUID, err error) {
	timeUUID = gocql.TimeUUID()

	err = c.session.Query(`
	CREATE TABLE IF NOT EXISTS assignment.messages (
		uuid uuid,
		created_at timestamp,
		email varchar,
		title varchar,
		content varchar,
		magic_number int,
		PRIMARY KEY(uuid)
	);`).Exec()
	if err != nil {
		return
	}
	timestamp := time.Now()
	err = c.session.Query(`INSERT INTO assignment.messages (uuid, created_at, email, title, content, magic_number) VALUES (?, ?, ?, ?, ?, ?)`,
		timeUUID, timestamp, msg.Email, msg.Title, msg.Content, msg.MagicNumber).Exec()

	return
}

// GetPaginatedMessagesByEmail reads single email message from database
func (c *connection) GetPaginatedMessagesByEmail(requestedEmail string, ps []byte, limit *int64) ([]*models.Message, []byte, error) {
	var (
		magicNumber           int
		content, email, title string
		limitVal              int
		targetMessages        []*models.Message
		uuid                  gocql.UUID
	)
	query := c.session.Query(`
	SELECT email, uuid, title, content, magic_number
	FROM messages WHERE email = ? ALLOW FILTERING`, requestedEmail)

	if limit == nil {
		limitVal = 5
	} else {
		limitVal = int(*limit)
	}

	iter := query.PageState(ps).PageSize(limitVal).Iter()
	for iter.Scan(&email, &uuid, &title, &content, &magicNumber) {
		var (
			mn int64      = int64(magicNumber)
			t  string     = title
			e  string     = email
			c  string     = content
			u  gocql.UUID = uuid
		)
		targetMessages = append(targetMessages, &models.Message{
			UUID:        u.String(),
			MagicNumber: &mn,
			Content:     c,
			Email:       &e,
			Title:       &t,
		})
	}

	pageState := iter.PageState()

	if err := iter.Close(); err != nil {
		return nil, nil, err
	}

	return targetMessages, pageState, nil
}

func (c *connection) GetMessagesByMagicNumber(mn int64) ([]*models.Message, error) {
	var (
		magicNumber           int
		content, email, title string
		targetMessages        []*models.Message
		uuid                  gocql.UUID
	)
	iter := c.session.Query(`
	SELECT email, uuid, title, content, magic_number
	FROM messages WHERE magic_number = ? ALLOW FILTERING
	`, mn).Iter()

	for iter.Scan(&email, &uuid, &title, &content, &magicNumber) {
		var (
			mn int64      = int64(magicNumber)
			t  string     = title
			e  string     = email
			c  string     = content
			u  gocql.UUID = uuid
		)
		targetMessages = append(targetMessages, &models.Message{
			UUID:        u.String(),
			MagicNumber: &mn,
			Content:     c,
			Email:       &e,
			Title:       &t,
		})
	}

	if err := iter.Close(); err != nil {
		return nil, err
	}

	return targetMessages, nil
}

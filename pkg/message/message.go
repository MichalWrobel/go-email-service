package message

import (
	"acaisoft/swagger/models"

	"github.com/gocql/gocql"
)

// Reader read DB interface
type Reader interface {
	GetPaginatedMessagesByEmail(emailAddress string, pageState []byte, limit *int64) ([]*models.Message, []byte, error)
	GetMessagesByMagicNumber(mn int64) ([]*models.Message, error)
}

// Writer write DB interface
type Writer interface {
	InsertMessage(msg *models.Message) (timeUUID gocql.UUID, err error)
}

// Messages interface
type Messages interface {
	Reader
	Writer
}

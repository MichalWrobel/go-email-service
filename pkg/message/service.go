package message

import (
	"fmt"
	"net/smtp"
	"os"
)

// MailService interface
type MailService interface {
	SendMessagesByMagicNumber(magicNumber int64) error
}

type service struct {
	service  MailService
	messages Messages
}

// NewMessageService returns mail service instance
func NewMessageService(messages Messages) MailService {
	return service{
		messages: messages,
	}
}

// SendMessagesByMagicNumber sends all messages with the same magic number
func (s service) SendMessagesByMagicNumber(number int64) error {
	var (
		user     string = os.Getenv("SMTP_USER")
		password string = os.Getenv("SMTP_PASSWORD")
		host     string = os.Getenv("SMTP_HOST")
		port     string = os.Getenv("SMTP_PORT")
	)
	hostAndPort := fmt.Sprintf("%s:%s", host, port)
	auth := smtp.PlainAuth("", user, password, host)
	messages, err := s.messages.GetMessagesByMagicNumber(number)
	if err != nil {
		return err
	}

	c := make(chan string, len(messages))
	go func() {
		for _, m := range messages {
			id, err := sendMail(hostAndPort, user, *m.Email, m.Content, auth)
			if err != nil {
				fmt.Printf("Error seding email: %v", err)
			} else {
				c <- id
			}
		}
		close(c)
	}()

	return nil
}

func sendMail(hostAndPort, user, to, msg string, auth smtp.Auth) (string, error) {
	err := smtp.SendMail(hostAndPort, auth, user, []string{to}, []byte(msg))
	if err != nil {
		return "", err
	}
	id := fmt.Sprintf("%s;%s;%s;%s", hostAndPort, user, to, msg)

	return id, nil
}

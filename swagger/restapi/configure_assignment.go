// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	custom_api "acaisoft/api"
	"acaisoft/pkg/message"
	"crypto/tls"
	"fmt"
	"net/http"

	"acaisoft/swagger/restapi/operations"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
)

//go:generate swagger generate server --target ../../swagger --name Assignment --spec ../swagger.yml

func configureFlags(api *operations.AssignmentAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.AssignmentAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf
	messages, err := message.NewCassandraConnection()
	if err != nil {
		panic(fmt.Errorf("DB error: %v", err))
	}

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.GetMessageHandler = custom_api.GetMessageHandler(messages)
	api.PostMessageHandler = custom_api.PostMessageHandler(messages)
	api.SendMessageHandler = custom_api.SendMessageHandler(messages)

	if api.GetMessageHandler == nil {
		api.GetMessageHandler = operations.GetMessageHandlerFunc(func(params operations.GetMessageParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.GetMessage has not yet been implemented")
		})
	}
	if api.PostMessageHandler == nil {
		api.PostMessageHandler = operations.PostMessageHandlerFunc(func(params operations.PostMessageParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.PostMessage has not yet been implemented")
		})
	}
	if api.SendMessageHandler == nil {
		api.SendMessageHandler = operations.SendMessageHandlerFunc(func(params operations.SendMessageParams) middleware.Responder {
			return middleware.NotImplemented("operation operations.SendMessage has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}

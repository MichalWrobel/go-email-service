// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// SendMessageHandlerFunc turns a function with the right signature into a send message handler
type SendMessageHandlerFunc func(SendMessageParams) middleware.Responder

// Handle executing the request and returning a response
func (fn SendMessageHandlerFunc) Handle(params SendMessageParams) middleware.Responder {
	return fn(params)
}

// SendMessageHandler interface for that can handle valid send message params
type SendMessageHandler interface {
	Handle(SendMessageParams) middleware.Responder
}

// NewSendMessage creates a new http.Handler for the send message operation
func NewSendMessage(ctx *middleware.Context, handler SendMessageHandler) *SendMessage {
	return &SendMessage{Context: ctx, Handler: handler}
}

/*SendMessage swagger:route POST /api/send sendMessage

Sends messages

*/
type SendMessage struct {
	Context *middleware.Context
	Handler SendMessageHandler
}

func (o *SendMessage) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewSendMessageParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}

// SendMessageBadRequestBody send message bad request body
//
// swagger:model SendMessageBadRequestBody
type SendMessageBadRequestBody struct {

	// error
	Error string `json:"error,omitempty"`

	// message
	Message string `json:"message,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this send message bad request body
func (o *SendMessageBadRequestBody) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *SendMessageBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *SendMessageBadRequestBody) UnmarshalBinary(b []byte) error {
	var res SendMessageBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// SendMessageBody send message body
//
// swagger:model SendMessageBody
type SendMessageBody struct {

	// magic number
	MagicNumber int64 `json:"magic_number,omitempty"`
}

// Validate validates this send message body
func (o *SendMessageBody) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *SendMessageBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *SendMessageBody) UnmarshalBinary(b []byte) error {
	var res SendMessageBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// SendMessageDefaultBody send message default body
//
// swagger:model SendMessageDefaultBody
type SendMessageDefaultBody struct {

	// error
	Error *string `json:"error,omitempty"`

	// message
	Message *string `json:"message,omitempty"`

	// status
	Status *string `json:"status,omitempty"`
}

// Validate validates this send message default body
func (o *SendMessageDefaultBody) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *SendMessageDefaultBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *SendMessageDefaultBody) UnmarshalBinary(b []byte) error {
	var res SendMessageDefaultBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// SendMessageOKBody send message o k body
//
// swagger:model SendMessageOKBody
type SendMessageOKBody struct {

	// magic number
	MagicNumber int64 `json:"magic_number,omitempty"`

	// message
	Message string `json:"message,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this send message o k body
func (o *SendMessageOKBody) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (o *SendMessageOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *SendMessageOKBody) UnmarshalBinary(b []byte) error {
	var res SendMessageOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

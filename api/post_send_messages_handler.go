package api

import (
	"acaisoft/pkg/message"
	"acaisoft/swagger/restapi/operations"
	"fmt"

	"github.com/go-openapi/runtime/middleware"
)

// SendMessageHandler handles post message requests
func SendMessageHandler(messagesInterface message.Messages) operations.SendMessageHandler {
	defer func() {
		if r := recover(); r != nil {
			doSendMessagesError(fmt.Sprintf("Error: %v", r))
		}
	}()
	return operations.SendMessageHandlerFunc(func(params operations.SendMessageParams) (responder middleware.Responder) {
		mailService := message.NewMessageService(messagesInterface)
		mailService.SendMessagesByMagicNumber(params.SendParams.MagicNumber)
		return doSendMessagesOK(params.SendParams.MagicNumber)
	})
}

func doSendMessagesBadRequest(msg string) middleware.Responder {
	response := operations.NewSendMessageBadRequest()
	shape := operations.SendMessageBadRequestBody{
		Error:   msg,
		Message: "Bad request",
		Status:  "400",
	}
	response.SetPayload(&shape)
	return response
}

func doSendMessagesError(msg string) middleware.Responder {
	response := operations.NewSendMessageDefault(500)
	errMsg := "Error"
	statusCode := "500"
	shape := operations.SendMessageDefaultBody{
		Error:   &msg,
		Message: &errMsg,
		Status:  &statusCode,
	}

	response.SetPayload(&shape)
	return response
}

func doSendMessagesOK(mnumber int64) middleware.Responder {
	response := operations.NewSendMessageOK()
	body := operations.SendMessageOKBody{
		MagicNumber: mnumber,
		Message:     "Success",
		Status:      "200",
	}
	response.SetPayload(&body)
	return response
}

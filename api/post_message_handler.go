package api

import (
	"acaisoft/pkg/message"
	"acaisoft/swagger/restapi/operations"
	"fmt"

	"github.com/go-openapi/runtime/middleware"
)

// PostMessageHandler handles post message requests
func PostMessageHandler(messagesInterface message.Messages) operations.PostMessageHandler {
	defer func() {
		if r := recover(); r != nil {
			doPostMessageError(fmt.Sprintf("Error: %v", r))
		}
	}()
	return operations.PostMessageHandlerFunc(func(params operations.PostMessageParams) (responder middleware.Responder) {
		if len(params.MessageParams.UUID) > 0 {
			return doPostMessageBadRequest("Invalid request.")
		}
		uuid, err := messagesInterface.InsertMessage(params.MessageParams)
		if err != nil {
			return doPostMessageBadRequest(fmt.Sprintf("An unexpected error occured: %v", err))
		}
		return doPostMessageOK(uuid.String())
	})
}

func doPostMessageBadRequest(msg string) middleware.Responder {
	response := operations.NewPostMessageBadRequest()
	shape := operations.PostMessageBadRequestBody{
		Error:   msg,
		Message: "Bad request",
		Status:  "400",
	}
	response.SetPayload(&shape)
	return response
}

func doPostMessageError(msg string) middleware.Responder {
	response := operations.NewPostMessageDefault(500)
	errMsg := "Error"
	statusCode := "500"
	shape := operations.PostMessageDefaultBody{
		Error:   &msg,
		Message: &errMsg,
		Status:  &statusCode,
	}

	response.SetPayload(&shape)
	return response
}

func doPostMessageOK(uuid string) middleware.Responder {
	response := operations.NewPostMessageOK()
	data := operations.PostMessageOKBodyData{
		UUID: uuid,
	}
	body := operations.PostMessageOKBody{
		Data:    &data,
		Message: "Success",
		Status:  "200",
	}
	response.SetPayload(&body)
	return response
}

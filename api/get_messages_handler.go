package api

import (
	"acaisoft/pkg/message"
	"acaisoft/swagger/models"
	"acaisoft/swagger/restapi/operations"
	"encoding/hex"

	"github.com/go-openapi/runtime/middleware"
)

// GetMessageHandler handles get messages requests
func GetMessageHandler(messagesInterface message.Messages) operations.GetMessageHandler {
	return operations.GetMessageHandlerFunc(func(params operations.GetMessageParams) (responder middleware.Responder) {
		page := []byte{}
		var err error
		if params.Page != nil {
			page, err = hex.DecodeString(*params.Page)
			if err != nil {
				operations.NewGetMessageDefault(500)
			}
		}
		messages, pageState, err := messagesInterface.GetPaginatedMessagesByEmail(params.Email, page, params.Limit)
		if err != nil {
			operations.NewGetMessageDefault(500)
		}
		return doGetMessageOK(messages, params, pageState)
	})
}

func doGetMessageOK(messages []*models.Message, params operations.GetMessageParams, pageState []byte) middleware.Responder {
	var (
		body  operations.GetMessageOKBody
		limit int64 = 5
	)
	data := operations.GetMessageOKBodyData{
		Messages: messages,
	}
	if params.Limit != nil {
		limit = *params.Limit
	}
	body = operations.GetMessageOKBody{
		Data:    &data,
		Message: "Success",
		Status:  "200",
		Page:    hex.EncodeToString(pageState),
		Limit:   limit,
	}

	response := operations.NewGetMessageOK()
	response.SetPayload(&body)
	return response
}
